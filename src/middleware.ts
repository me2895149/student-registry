import express, { Request, Response, NextFunction  } from "express";

export const logger = (req: Request, res: Response, next: NextFunction) => {
    console.log(new Date())
    console.log(req.method)
    console.log(req.hostname)
    console.log(req.body)
    next()
}

export const errorMiddleware = (_req: Request, res: Response) => {
    res.status(404).send({ error: 'There is nothing to show here!!!'});
    }

export const validate = (req: Request, res: Response, next: NextFunction ) => {
    const { id, name, email } = req.body
    if (typeof(id) !== 'string' || typeof(name) !== 'string' || typeof(email) !== 'string') {
        return res.status(400).send('Missing or invalid parameters')
    }
    next()
}
    