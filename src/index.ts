import express, { Request, Response, NextFunction  } from "express"
import { logger, errorMiddleware, validate } from "./middleware"

const server = express()

interface Student {
    id: string
    name: string
    email: string
}

server.use(express.json())
const students: Array<Student> = []
server.use(logger)

server.get('/students', (req: Request, res: Response) => {
    const ids = students.map(student => student.id)
    res.send(ids)
})

/*server.post('/', (req: Request, res: Response) => {
    res.send('POST')
})
*/

server.get('/students/:id', (req: Request, res: Response) => {
    const input = req.params.id
    const student = students.find(student => student.id === input)
    if (student == undefined) {
        res.status(404).send('Student not found!')
    }
    res.send(student)
})

server.post('/students', validate, (req: Request, res: Response) => {
    const student: Student = req.body
    students.push(student)
    res.send('res.status(201)')
})

server.put('/students/:id', (req: Request, res: Response) => {
    let student = students.find(student => student.id === req.params.id)
    if (student == undefined) {
        res.status(404).send('Student not found!')
    }
    else {
            if (req.body.id !== undefined) student.id = req.body.id
            if (req.body.name !== undefined) student.name = req.body.name
            if (req.body.email !== undefined) student.email = req.body.email
        }
    res.status(204).send("")
})

server.delete('/students/:id', (req: Request, res: Response) => {
    const input = req.params.id
    const student = students.findIndex(student => student.id === input)
    if (student == undefined) {
        res.status(404).send('Student not found!')
    }
    students.splice(student, +(input))
    res.status(204).send("")
})

server.use(errorMiddleware)

server.listen(3000)